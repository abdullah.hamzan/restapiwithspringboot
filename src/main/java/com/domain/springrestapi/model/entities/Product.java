package com.domain.springrestapi.model.entities;


import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "tbl_product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long price;

    @ManyToOne
    @JoinColumn(name="category_id", nullable=false, referencedColumnName = "id")
    private Category category;



}
