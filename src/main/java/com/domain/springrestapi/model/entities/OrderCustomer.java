package com.domain.springrestapi.model.entities;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_customers")
public class OrderCustomer {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "constumers_id",referencedColumnName = "id")
    private Constumer constumers;

    @OneToOne
    @JoinColumn(name = "orders_id",referencedColumnName = "id")
    private Order orders;





}
