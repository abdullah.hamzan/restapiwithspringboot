package com.domain.springrestapi.model.entities;



import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "categories")
@Data
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;


    private String description;

    @JsonIgnore
    @OneToMany(mappedBy="category")
    private Set<Product> products;






}
