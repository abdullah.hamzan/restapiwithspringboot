package com.domain.springrestapi.model.repository;

import com.domain.springrestapi.model.entities.Constumer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ConstumerRepository extends JpaRepository<Constumer,Long> {


}
