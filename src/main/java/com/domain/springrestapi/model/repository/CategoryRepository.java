package com.domain.springrestapi.model.repository;

import com.domain.springrestapi.model.entities.Category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query("select count(*) from Category")
    public int categoryCount();



}
