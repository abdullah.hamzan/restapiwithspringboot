package com.domain.springrestapi.model.repository;

import com.domain.springrestapi.model.entities.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post,Long> {




}
