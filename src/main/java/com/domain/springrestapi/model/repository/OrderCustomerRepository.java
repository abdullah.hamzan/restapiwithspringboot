package com.domain.springrestapi.model.repository;


import com.domain.springrestapi.model.entities.OrderCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderCustomerRepository extends JpaRepository<OrderCustomer,Long> {


}
