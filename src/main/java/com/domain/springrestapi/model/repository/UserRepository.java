package com.domain.springrestapi.model.repository;

import com.domain.springrestapi.model.entities.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {



}