package com.domain.springrestapi.model.repository;

import com.domain.springrestapi.model.entities.Product;

import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepository extends JpaRepository<Product, Long> {

}
