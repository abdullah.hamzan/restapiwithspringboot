package com.domain.springrestapi.services;

import com.domain.springrestapi.model.entities.Post;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;
import java.util.Optional;


public interface PostService {

    Post ceateData(Post post);
    List<Post> getData();
    Optional<Post> updateData(Post post, Long id);




}
