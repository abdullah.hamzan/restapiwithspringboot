package com.domain.springrestapi.services;



import com.domain.springrestapi.model.entities.OrderCustomer;

import java.util.List;

public interface OrderCustomerService {


    OrderCustomer createData (OrderCustomer orderCustomer);

    List<OrderCustomer> getDataOrderCustomer ();

}
