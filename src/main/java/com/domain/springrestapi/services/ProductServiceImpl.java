package com.domain.springrestapi.services;

import java.util.List;

import com.domain.springrestapi.model.entities.Product;
import com.domain.springrestapi.model.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product getProduct(Long id) {

        return productRepository.findById(id).orElse(null);
    }

    @Override
    public Iterable<Product> getAllProduct() {
        return productRepository.findAll();
    }

}
