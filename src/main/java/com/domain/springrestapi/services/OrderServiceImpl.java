package com.domain.springrestapi.services;

import com.domain.springrestapi.model.entities.Order;
import com.domain.springrestapi.model.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired(required = true)
    OrderRepository orderRepository;

    @Override
    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public List<Order> getDataOrder() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Order> updateOerder(@RequestBody Order order, Long id) {
        Optional<Order>  orderById = orderRepository.findById(id);

        orderById.ifPresent(res->{
            res.setName(order.getName());
            orderRepository.save(res);
        });

        return orderById;


    }
}
