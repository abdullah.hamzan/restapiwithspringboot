package com.domain.springrestapi.services;

import com.domain.springrestapi.model.entities.Product;

public interface ProductService {



    Product createProduct(Product product);
    Product getProduct(Long id);
    Iterable<Product> getAllProduct();

}
