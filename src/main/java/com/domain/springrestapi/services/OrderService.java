package com.domain.springrestapi.services;


import com.domain.springrestapi.model.entities.Order;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface OrderService {

    Order createOrder(Order order);

    List<Order> getDataOrder();

    Optional<Order> updateOerder(Order order, Long id);
}
