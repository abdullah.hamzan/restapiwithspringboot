package com.domain.springrestapi.services;

import com.domain.springrestapi.model.entities.Post;
import com.domain.springrestapi.model.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    PostRepository postRepository;
    @Override
    public Post ceateData(@RequestBody Post post) {
        return postRepository.save(post);

    }

    @Override
    public List<Post> getData() {
        return postRepository.findAll();
    }

    @Override
    public Optional<Post> updateData(@RequestBody Post post, Long id) {
        Optional<Post> postDataById= postRepository.findById(id);

        postDataById.ifPresent(res->{
            res.setTitle(post.getTitle());
            res.setDescription(post.getDescription());

            postRepository.save(res);
        });


        return postDataById;





    }
}
