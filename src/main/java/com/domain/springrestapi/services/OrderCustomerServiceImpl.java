package com.domain.springrestapi.services;


import com.domain.springrestapi.model.entities.OrderCustomer;
import com.domain.springrestapi.model.repository.OrderCustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class OrderCustomerServiceImpl implements OrderCustomerService {


    @Autowired(required = true)
    OrderCustomerRepository orderCustomerRepository;

    @Override
    public OrderCustomer createData(OrderCustomer orderCustomer) {

        return orderCustomerRepository.save(orderCustomer);

    }

    @Override
    public List<OrderCustomer> getDataOrderCustomer() {
        return orderCustomerRepository.findAll();
    }
}
