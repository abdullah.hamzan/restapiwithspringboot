package com.domain.springrestapi.services;

import com.domain.springrestapi.model.entities.Constumer;
import com.domain.springrestapi.model.repository.ConstumerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class ConstumerServiceImpl implements ConstumerService {

    @Autowired(required = true)
    ConstumerRepository constumerRepository;

    @Override
    public Constumer createConstumer(@RequestBody Constumer constumer) {
        return constumerRepository.save(constumer);
    }

    @Override
    public List<Constumer> getDataConstumer() {
        return constumerRepository.findAll();
    }
}
