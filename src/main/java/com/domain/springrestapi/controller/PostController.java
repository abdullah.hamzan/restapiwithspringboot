package com.domain.springrestapi.controller;


import com.domain.springrestapi.model.entities.Post;
import com.domain.springrestapi.services.PostServiceImpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/posts")
public class PostController {


    @Autowired
    PostServiceImpl postService;

    @PostMapping
    public Post createDataPost(@RequestBody Post post){

       return postService.ceateData(post);


    }

    @GetMapping
    public List<Post> indexDataPost(){

        return postService.getData();


    }


    @PutMapping("/{id}")
    public Optional<Post> updateDataPost(@RequestBody Post post, @PathVariable(value = "id") Long id){

        return postService.updateData(post,id);



    }









}
