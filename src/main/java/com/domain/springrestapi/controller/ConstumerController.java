package com.domain.springrestapi.controller;

import com.domain.springrestapi.model.entities.Constumer;
import com.domain.springrestapi.services.ConstumerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/constumers")
public class ConstumerController {

   @Autowired(required = true)
    ConstumerServiceImpl constumerService;

    @PostMapping
    public Constumer postData(@RequestBody Constumer constumer){

        return constumerService.createConstumer(constumer);
    }

    @GetMapping
    public List<Constumer> getData(){

        return constumerService.getDataConstumer();
    }

}
