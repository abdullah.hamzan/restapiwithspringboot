package com.domain.springrestapi.controller;

import com.domain.springrestapi.model.entities.Product;
import com.domain.springrestapi.services.ProductService;

import com.domain.springrestapi.services.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductServiceImpl productService;


    @PostMapping
   public Product postProduct(@RequestBody Product product){

       return productService.createProduct(product);

   }



    @GetMapping("/{id}")
    Product getProductById(@PathVariable("id") Long id) {

        System.out.println("id:" + id);
        return productService.getProduct(id);

    }

    @GetMapping("/")
    Iterable<Product> getProductAll() {
        return productService.getAllProduct();

    }

    @PostMapping("/")
    public String testRoute() {
        return "berhasil";
    }

}
