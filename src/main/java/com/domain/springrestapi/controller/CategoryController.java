package com.domain.springrestapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import com.domain.springrestapi.model.entities.Category;
import com.domain.springrestapi.model.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/categori")
public class CategoryController {

    @Autowired(required = true)
    private CategoryRepository categoryRepository;

    @CrossOrigin
    @GetMapping("/all")
    public List<Category> getAllCategori() {

        return categoryRepository.findAll();

    }



    @GetMapping("/all/count")
    public int countAllCategori() {

        return categoryRepository.categoryCount();

    }

    @GetMapping("/all/{id}")
    public Optional<Category> getOneCategori(@PathVariable(value = "id") Long id) {

        return categoryRepository.findById(id);

    }

    @PostMapping("/all")
    public Category createCategori(@RequestBody Category payload) {

        return categoryRepository.save(payload);

    }

    @PutMapping("/all/{id}")
    public Optional<Category> updateOneCategori(@PathVariable(value = "id") Long id, @RequestBody Category category) {

        Optional<Category> catgoryById = categoryRepository.findById(id);
        catgoryById.ifPresent(res -> {
            res.setName(category.getName());
            res.setDescription(category.getDescription());
            categoryRepository.save(res);
        });

        return catgoryById;

    }

    @DeleteMapping("/all/{id}")
    public void deleteOneCategori(@PathVariable(value = "id") Long id) {

        Optional<Category> categoryById = categoryRepository.findById(id);
        categoryById.ifPresent(res -> {

            categoryRepository.delete(res);

        });

    }

}
