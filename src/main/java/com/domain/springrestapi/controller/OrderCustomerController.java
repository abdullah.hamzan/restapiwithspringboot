package com.domain.springrestapi.controller;


import com.domain.springrestapi.model.entities.OrderCustomer;
import com.domain.springrestapi.services.OrderCustomerServiceImpl;



import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/order_custumer")
@RestController
@Slf4j
public class OrderCustomerController {

    @Autowired(required = true)
    OrderCustomerServiceImpl orderCustomerService;

    @PostMapping("/add")
    public OrderCustomer postOrderCustomer(@RequestBody OrderCustomer orderCustomer){


         log.info(orderCustomer.toString());
        return orderCustomerService.createData(orderCustomer);

    }

    @GetMapping("/get")
    public List<OrderCustomer> getOrderCustomer(){
        return orderCustomerService.getDataOrderCustomer();

    }


}
