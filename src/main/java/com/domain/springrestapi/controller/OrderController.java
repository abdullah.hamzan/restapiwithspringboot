package com.domain.springrestapi.controller;


import com.domain.springrestapi.model.entities.Order;

import com.domain.springrestapi.services.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;


@RequestMapping("/api/orders")
@RestController
public class OrderController {

    @Autowired(required = true)
    OrderServiceImpl orderService;

    @PostMapping
    public Order postData(@RequestBody Order order){

        return orderService.createOrder(order);
    }


    @GetMapping
    public List<Order> getData(){

        return orderService.getDataOrder();
    }

    @PutMapping("/{id}")
    public Optional<Order> updateData(@RequestBody Order order,@PathVariable(value = "id") Long id){

        return orderService.updateOerder(order,id);
    }
}
